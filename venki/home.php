<!DOCTYPE html>
<html lang="en">
<head>
  <title>Website Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
  <style>
    /* Remove the navbar's default margin-bottom and rounded borders */
    .navbar {
      margin-bottom: 0;
      border-radius: 0;
    }
    
    /* Set height of the grid so .sidenav can be 100% (adjust as needed) */
    .row.content {height: 850px}
    
    /* Set gray background color and 100% height */
    .sidenav {
      padding-top: 60px;
      background-color: #f1f1f1;
      height: 100%;
    }
    
    /* Set black background color, white text and some padding */
    footer {
      background-color: #555;
      color: white;
      padding: 0px;
    }
    
    /* On small screens, set height to 'auto' for sidenav and grid */
    @media screen and (max-width: 767px) {
      .sidenav {
        height: auto;
        padding: 15px;
      }
      .row.content {height:auto;}
    }
  </style>
  <style> 
div #div1 {
    width: 100%;
    height: 5em;
    background-color: red;
    -webkit-animation-name: example; /* Chrome, Safari, Opera */
    -webkit-animation-duration: 4s; /* Chrome, Safari, Opera */
    -webkit-animation-iteration-count: 100; /* Chrome, Safari, Opera */
    animation-name: example;
    animation-duration: 4s;
    animation-iteration-count: 100;
}

/* Chrome, Safari, Opera */
@-webkit-keyframes example {
    0%   {background-color: red;}
    50%  {background-color: blue;}
    100% {background-color: green;}
}

/* Standard syntax */
@keyframes example {
    0%   {background-color: red;}
    
    50%  {background-color: blue;}
    100% {background-color: green;}
}


div#div10 {
      -moz-animation-duration: 25s;
      -webkit-animation-duration: 25s;
      -moz-animation-name: zoom;
      -webkit-animation-name: zoom;
      -moz-animation-iteration-count: infinite;
      -webkit-animation-iteration-count: infinite;
      -moz-animation-direction: alternate;
      -webkit-animation-direction: alternate;
    }
    
    @-moz-keyframes zoom {
      from {
        font-size:100%;
      }
      
      30% {
        font-size:200%;
      }
      70% {
        font-size:200%;
      }
      
      to {
       font-size:100%;
      }
    }
    
    @-webkit-keyframes zoom {
      from {
        font-size:100%;
      }
      
        30% {
        font-size:200%;
      }
      70% {
        font-size:200%;
      }
      
      
      to {
        font-size:100%;
        
      }
    }

  </style>


</head>

<body>
<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>

      </button>
      <a class="navbar-brand"><img width="300" height="" src="fheader_logo.png" title="" alt="Cloud To Cloud Solutions"/></a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
        <li class="active"><a href="#">Home</a></li>
        <li><a href="#">About</a></li>
        <li><a href="#">Projects</a></li>
        <li><a href="#">Contact</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li><a href="#"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
        <li><a href="#"><span class="glyphicon glyphicon-log-in"></span> Logout</a></li>
      </ul>
    </div>
  </div>
</nav>

        


<div data-spy="scroll" data-target="#myScrollspy" data-offset="50">
<div class="container-fluid text-center">
  <div class="row content" >
    <div class="col-sm-2 sidenav" id="myScrollspy">
      <h4><p><a href="#">Link</a></p>
      <p><a href="#">Link</a></p>
      <p><a href="#">Link</a></p>
      <p><a href="#">Link</a></p>
      <p><a href="#">Link</a></p>
      <p><a href="#">Link</a></p>
      <p><a href="#">Link</a></p>
      <p><a href="#">Link</a></p>
      
    </h4>
    </div>
    </div>
    </div>
  <br>
      <div class="container">
   <div id="myCarousel" class="carousel slide">
  
      <!-- Indicators -->
        <ol class="carousel-indicators">
          <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
          <li data-target="#myCarousel" data-slide-to="1"></li>
        
           <li data-target="#myCarousel" data-slide-to="2"></li>
  
        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner">
          <div class="item active">
            <div class="fill" style="background-image:url(https://farm8.staticflickr.com/7516/15996679602_149ab2581b_o.jpg);"></div>
            
          </div>
          <div class="item">
            <div class="fill" style="background-image:url(https://farm8.staticflickr.com/7462/15995372001_25ac47210c_o.jpg);"></div>
           
          </div>
          <div class="item">
            <div class="fill" style="background-image:url(https://farm8.staticflickr.com/7485/15811617387_4edda803c2_o.jpg);"></div>
            
          </div>
          
         
        </div>

        <!-- Controls -->
     
        <a class="left carousel-control" href="#myCarousel" data-slide="prev">
          <span class="icon-prev"></span>
        </a>
        <a class="right carousel-control" href="#myCarousel" data-slide="next">
          <span class="icon-next"></span>
        </a>
    </div>
</div>
 <div class="container">
     <div class="row well">
          <div class="col-lg-4 col-md-4">
            <h3><i class="icon-user"></i> User Specific Application </h3>
            <p>We have world class experts in user specific application design and development. On cost, On time future projective technologies implementation. </p>
          </div>
          <div class="col-lg-4 col-md-4">
            <h3><i class="icon-tablet"></i> Single Window Design </h3>
         <p>

          No need to go for other domains, everything on your home page, Integrated single window for all IT needs and business.
      </p>
          </div>
          <div class="col-lg-4 col-md-4">
            <h3><i class="icon-cloud"></i> On Demand Services</h3>
            <p>
            Only pay for what you use. No fixed terms always decision is yours. SaaS, NaaS, PaaS, IaaS implementation in various business logics.

</p>
          </div>
          </div>
          </div>

  <!--   <div class="col-sm-8 text-left">
      <h1>Welcome</h1>
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
      <hr>
      <h3>Test</h3>
      <p>Lorem ipsum...</p>
    </div>
    <div class="col-sm-2 sidenav">
      <div class="well">
        <p>ADS</p>
      </div>
      <div class="well">
        <p>ADS</p>
      </div>
    </div> -->


<footer>

  <div align="center"  class="row">
          <div class="col-lg-12">
      
          <ul class="list-unstyled list-inline list-social-icons">
            <li class="tooltip-social facebook-link">
            <a href="https://www.facebook.com/ctcsindia" target="_blank" data-toggle="tooltip" 
            data-placement="top" title="Facebook">
            <i class="icon-facebook-sign icon-2x"></i>
            </a>
            </li>
            <li class="tooltip-social twitter-link">
            <a href="https://twitter.com/CTCSindia" target="_blank" data-toggle="tooltip" 
            data-placement="top" title="Twitter">
            <i class="icon-twitter-sign icon-2x"></i>
            </a>
            </li>
      <li class="tooltip-social youtube-link">
            <a href="https://www.youtube.com/channel/UCEePuUFbuRk-XpEu0oh8Dzg" target="_blank" 
            data-toggle="tooltip" data-placement="top" title="Youtube">
            <i class="icon-youtube-sign icon-2x"></i>
            </a>
            </li>
            </ul>
            <p>Copyright &copy; CTCS 2016</p>
          </div>
        </div>
</footer>

</body>
</html>

