<!DOCTYPE html>
<html>
	<head>
	<title>PCS | CTCS | PONDICHERRY</title>
	<meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="shortcut icon" href="logo/CTCS_Final_Logo.png"/>
	<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="css/bootstrap-responsive.css">
        <link rel="stylesheet" type="text/css" href="css/bootstrap-responsive.min.css">
        <link rel="stylesheet" type="text/css" href="css/bootstrap-theme.css">
        <link rel="stylesheet" type="text/css" href="css/bootstrap-theme.min.css">
        <link rel="stylesheet" type="text/css" href="css/datepicker.css">
        <link rel="stylesheet" type="text/css" href="css/pcs.css">
        <link rel="stylesheet" href="css/jquery-ui.css">        
        <link rel="stylesheet" href="/resources/demos/style.css">
        <link rel="stylesheet" type="text/css" href="css/demo.css" />
        <link rel="stylesheet" type="text/css" href="css/style3.css" />
        <script type="text/javascript" src="js/modernizr.custom.86080.js"></script>
        <script src="js/jquery-1.10.1.min.js"></script>
	<script src="js/bootstrap.js"></script>
        <script src="js/jquery-1.9.1.js"></script>
        <script src="js/jquery-ui.js"></script>
        <style>
        #setframe{
            width: 320px;
            height: 180px;
            background-color: #D0D0D0 ;
            box-shadow: 10px 10px 5px #888888;
        }
    #setbgc{
        /* background: linear-gradient(to right,green,yellow); */
    /* Chrome, Safari, Opera */
    background: #1E90FF;
    /*-webkit-animation-name: myfirst;
    -webkit-animation-duration: 15s;
    -webkit-animation-iteration-count: infinite;*/

    
    Chrome, Safari, Opera 
@-webkit-keyframes myfirst {
    25%  {background:#FF3030; }
    50%  {background:#FFD700; }
    75%  {background:#1E90FF;}
    100% {background:#FFA500;}
    100% {background:green;}
}

</style>

	</head>
	<body id="setbgc">
    <!--    <ul class="cb-slideshow">
            <li><span>Image 01</span><div></div></li>
            <li><span>Image 02</span><div></div></li>
            <li><span>Image 03</span><div></div></li>
            <li><span>Image 04</span><div></div></li>
            <li><span>Image 05</span><div></div></li>
            <li><span>Image 06</span><div></div></li>
        </ul>
        -->
      <div id="header1">
	<h1 align="center"><a href="" style="color:#FFF;text-shadow: 5px 5px 5px #A8A8A8 ;">
    <font face="Lucida Grande, Lucida Sans Unicode, Lucida Sans, DejaVu Sans, Verdana, sans-serif">Landscape</font></a></h1>
	<center>
    <div id="setframe">
    <div style="background-color:#335468">
	    <h4 style="color:#FFFF45">Sign in</h4> </div>
            <h5 style="text-align:left;padding:5px;font: italic bold 12px/15px Georgia, serif;">Welcome to Finance Cloud Solutions V6.0. To continue please type your username and password here.</h5>
            <form name="login" method="post" action="login.php">
    <table>
    <tr>
    <td>
    <div class="left-inner-addon">
   <i class="glyphicon glyphicon-user"></i>
	<input style="text-align:left" class="form-control" type="text" name="username" placeholder="Username" autofocus>
    </div>
    </td>
    </tr>
    <tr><td></td></tr>
    <tr>
    <td>
    <div class="left-inner-addon ">
     <i class="glyphicon glyphicon-eye-close"></i>
  	<input class="form-control" type="password" name="password" placeholder="Password">
    </div>
    </td>
    </tr>
    </table>
    <br><br><br>
  	<input class="btn btn-success btn active" type="submit" name="submit" value="Sign in">
	</form>
    </div>
    </center>
				
	

</div>
<div id="footer1">
<h5 align="center" style="color:#FFF">Developed by <a href="http://www.ctcs.in/" style="color:#3F0" ><i>CTCS</i></a> | &copy; 2014 | 0413-4201581 | 9626561581 <a href="mailto:support@ctcs.in" style="color:#FFF"> support@ctcs.in </a></h5>
</div>

	</body>
</html>
